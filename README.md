# Chordata OFX Testcube

[Openframeworks](https://openframeworks.cc/) app to test K-Ceptor using in a non X11 raspberry environment.

Tested in **ofx 0.10.0**. Other versions would work, but it is harder to make the app run outside X11, see [here](https://forum.openframeworks.cc/t/rpi3-buster-lite-of-0-11-0-cant-run-example/33947/15).

## Usage

- Place inside the `$(OFX_ROOT)/apps/myApps` folder 

- Compile by running `make`

- Install dependencies

```bash
sudo $(OFX_ROOT)/scripts/linux/debian/install_dependencies.sh
sudo apt-get install -yq vim tmux
```
- Run with

```bash
tmux new-session -s "kceptor_test" ./split_and_run.sh
```

## Run on boot

add `startup.sh` to your `.bashrc` by running the following command once **from this directory**

```bash
echo "source $(pwd)/startup.sh" >> ~/.bashrc
``` 

## Screenshots

![Chordata ofx testcube v1.0 capture](./ofx_testcube_capture.jpg)
![Chordata ofx testcube v1.0 in raspberry](./raspi_capture.jpg)
