#!/bin/bash

if [ $(tty) == /dev/tty1 ]; then
	echo Running CHORDATA TESTCUBE
	tmux new-session -s "kceptor_test" ./split_and_run.sh
fi

alias run_notochord_local='/home/pi/notochord/bin/notochord -y --scan localhost 6565'
