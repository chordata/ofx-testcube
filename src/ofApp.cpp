#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	std::cout << "listening for osc messages on port " << PORT << "\n";
		receiver.setup( PORT );

	last_msg = "Last message: (none)";


	model.loadModel("testcube.dae", false);
	model.setRotation(0, -90, 1, 0, 0);

	ofSetVerticalSync(true);

	// this uses depth information for occlusion
	// rather than always drawing things on top of each other
	ofEnableDepthTest();

	ofSetSmoothLighting(true);
	light.setAreaLight(140, 50);
    light.setDiffuseColor( ofFloatColor(2, 2, 2));
    light.setSpecularColor( ofFloatColor(.85*2, 0.5*2, .55*2));
    light.setPosition(50, 300, 100);

    light_fill.setAreaLight(140, 50);
    light_fill.setDiffuseColor( ofFloatColor(2, 2, 2));
    light_fill.setSpecularColor( ofFloatColor(0.5*2, .55*2, .85*2));
    light_fill.setPosition(-150, -300, 100);

    float width  = ofGetWidth() * .12;
    box.set( width* 0.8 );


    plane.set(1000, 1000);   ///dimensions for width and height in pixels
    plane.setPosition(0, -60, 0); /// position in x y z
    plane.setResolution(10, 10); /// this resolution (as columns and rows) is enough
    plane.rotateDeg(100, 1, 0, 0);

    global.rotateDeg(90, 0, 1, 0);
    global.rotateDeg(100, 1, 0, 0);
	global.setPosition(0, -60, 0);

}

//--------------------------------------------------------------
void ofApp::update(){
	model.update();

	while( receiver.hasWaitingMessages() ){
			char msg_string[16384];
			ofxOscMessage m;
			receiver.getNextMessage( &m );

			if (m.getAddress().rfind("/Chordata", 0) == 0) {
				if (m.getNumArgs() == 4){
					bool ffff = true;
					for ( int i=0; i<m.getNumArgs(); i++ ){
						if (m.getArgType( i ) != OFXOSC_TYPE_FLOAT ){
							ffff = false;
						}
					}

					if (ffff){
						//set the quaternion
						//ofQuaternion::ofQuaternion(float x, float y, float z, float w)
						float w = m.getArgAsFloat( 0 );
						float x = m.getArgAsFloat( 1 );
						float y = m.getArgAsFloat( 2 );
						float z = m.getArgAsFloat( 3 );

						quat.set( -x, y,-z, w );

						sprintf(msg_string, "NOTOCHORD Quat @ %s [ %f %f %f %f ]",
							m.getAddress().c_str(),
							w, x, y ,z);

						std::cout << msg_string << std::endl;

						sprintf(msg_string, "Last message:\n%s [ %f %f %f %f ]",
							m.getAddress().c_str(),
							w, x, y ,z);
						last_msg = msg_string;
						continue;
						// ofVec3f ofQuaternion::getEuler()
					}

				}//wrong num of args

			} //don't start with chordata

			//The message is not the type we need, dump it to console

			strcpy( msg_string, m.getAddress().c_str() );
			strcat( msg_string, ": " );

			for ( int i=0; i<m.getNumArgs(); i++ ){
				// get the argument type
				strcat( msg_string, m.getArgTypeName( i ).c_str() );
				strcat( msg_string, ":" );
				// display the argument - make sure we get the right type
				if( m.getArgType( i ) == OFXOSC_TYPE_INT32 )
					sprintf( msg_string, "%s%d ", msg_string, m.getArgAsInt32( i ) );
				else if( m.getArgType( i ) == OFXOSC_TYPE_FLOAT )
					sprintf( msg_string, "%s%f ", msg_string, m.getArgAsFloat( i ) );
				else if( m.getArgType( i ) == OFXOSC_TYPE_STRING )
					sprintf( msg_string, "%s\"%s\" ", msg_string, m.getArgAsString( i ) );
				else 
					strcat( msg_string, "unknown" );
			}

			std::cout << msg_string << std::endl;
			last_msg = msg_string;
		}

}

//--------------------------------------------------------------
void ofApp::draw(){
	ofColor colorOne(15, 15, 65);
    ofColor colorTwo(0, 0, 0);

    ofBackgroundGradient(colorOne, colorTwo, OF_GRADIENT_LINEAR);


	cam.begin();
		light.enable();
		light_fill.enable();

		ofPushMatrix();
		// box.rotateDeg(spinX, 0.0, 1.0, 0.0);
		box.setGlobalOrientation(quat);
		box.rotateDeg(100, 1, 0, 0);
		box.setPosition(0, -40, 0);
		light.draw();
		// box.rotateDeg(180, 0, 0, 1);
		// box.drawWireframe();
		// ofDrawGrid(10,10,false,false,true,false);
		box.transformGL();
		model.drawFaces();
		box.restoreTransformGL();
		// box.drawWireframe();
		// box.draw();

		light.disable();
		light_fill.disable();
		global.transformGL();

		ofDrawGridPlane(40,10,false);

		global.restoreTransformGL();
		// ofDrawGrid(float stepSize=1.25f, size_t numberOfSteps, bool labels=false, bool x=true, bool y=true, bool z=true)
		// plane.drawWireframe();

		ofPopMatrix();

	cam.end();

	light.enable();
	light_fill.enable();
	ofSetColor(255,255,255,255);
	ofFill();
	ofDrawBitmapString(last_msg, 20, ofGetHeight() - 40);
	ofDrawBitmapString("Chordata ofx test-cube v1.0\n---------------------------", 20, 20);


}


//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}
