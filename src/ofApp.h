#pragma once

#include "ofMain.h"

#define OF_ADDON_USING_OFXOSC

#include "ofxOsc.h"
#include "ofxAssimpModelLoader.h"

#define PORT 6565
#define NUM_MSG_STRINGS 20


class ofApp : public ofBaseApp {
	public:
		void setup();
		void update();
		void draw();
		
		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y);
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		
		ofPlanePrimitive plane;
		ofBoxPrimitive box;
		ofLight light; // creates a light and enables lighting
		ofLight light_fill; // creates a light and enables lighting
		ofEasyCam cam; // add mouse controls for camera movement

		ofNode 				global;

		std::string last_msg;
		
	private:
		ofxAssimpModelLoader model;
		
		ofQuaternion quat; 

		ofxOscReceiver	receiver;

		int				current_msg_string;
		std::string		msg_strings[NUM_MSG_STRINGS];
		float			timers[NUM_MSG_STRINGS];
		
		int				mouseX, mouseY;
		char			mouseButtonState[128];

};
